import numpy as np
import pandas as pd
from typing import *
import torch
import torch.nn as nn
from torch_geometric.data import Data
from torch_geometric.loader import DataLoader
from torchsummary import summary
import wandb
# modules
from models import *
from dataset import CrimeDatasetSW,TemporalGraphDataset, CrimeDataset, ASTGCNDataset
from parser import parser
from train_test import train, train_sw



def initEnv(random_seed=69)-> Dict:
    """
    function that initializes and return device, sets random seeed for repeatable results.
    """
    settings = vars(parser())
    if settings['wandb']:
        #wandb.login()
        run = wandb.init(project="JBG050", config=settings)
    settings['device'] = torch.device('cuda' if torch.cuda.is_available() else 'cpu')# if cuda gpu available use that
    settings['device'] = 'cpu'
    settings['random_seed'] = random_seed
    # set fixed random seed for repeatability
    np.random.seed(random_seed)
    torch.manual_seed(random_seed)
    print(f'initializing model using {settings["device"]}, random seed: {random_seed}')
    return settings



def main(settings: Dict):
    """
    main that runs train / eval based on given settings
    """
    data = torch.load('../data/raw/graph_data.pt')
    #data = data.to(settings['device'])
    #edge_index, edge_weight = data.edge_index.to(settings['device']), data.edge_attr.to(settings['device'])
    n_nodes = 5203  # Number of nodes in your graph
    # Generate all pairs of node indices
    node_indices = torch.arange(n_nodes)
    source_nodes = node_indices.repeat_interleave(n_nodes)
    target_nodes = node_indices.repeat(n_nodes)
    # Combine source and target node indices into an edge_index tensor
    edge_index = torch.stack([source_nodes, target_nodes], dim=0)
    print(f'edge index: {edge_index.shape} {edge_index.dtype}')

    #adj_matrix = np.load('../data/adj_matrix.np.npy')
    #print(f'shape of data:\nx = {data.x.shape}\ny = {data.y.shape}')
    x = data.x#.unsqueeze(0)
    y = data.y#.unsqueeze(0)
    #y = data.y.unsqueeze(-1)
    print(f'x: {x.shape}, y: {y.shape}')

    split_index = int(x.size(2) * 0.9)  # 90% of the time steps
    x_train = x[:, :split_index, :]
    x_test = x[:, split_index:, :]
    y_train = y[:, :split_index]
    y_test = y[:, split_index:]

    train_dataset = ASTGCNDataset(x_train, y_train, lookback=settings['lookback'], forecast=1)
    test_dataset = ASTGCNDataset(x_test, y_test, lookback=settings['lookback'], forecast=1)
    train_loader = DataLoader(train_dataset, batch_size=settings['batch_size'], shuffle=True)
    test_loader = DataLoader(test_dataset, batch_size=settings['batch_size'], shuffle=False)
    model = AttentionGCNSW(15, 8, 5)
    model = model.to(settings['device'])

    for x, y in train_loader:
        x = x.squeeze(0).permute(0,2,1)
        y = y.squeeze(2)
        print(f'iter: x={x.shape} y={y.shape}')
        p = model(x, edge_index)
        print(f'out: {p.shape}')
        break

    criterion = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=settings['learning_rate'], weight_decay=settings['weight_decay'], amsgrad=settings['amsgrad'])

    if settings['model_summary']:
        raise NotImplementedError
        summary(model, (24,13692, 16), device=settings['device'])

    if settings['val']:
        raise NotImplementedError

    # train & test model
    results = train_sw(settings, train_loader, optimizer, model, criterion)
    return results

if __name__ == '__main__':
    main(initEnv())
