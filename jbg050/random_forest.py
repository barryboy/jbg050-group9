# import libraries 
import pandas as pd

import sklearn as sk
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error as mse

import argparse


def get_data():
    # import data
    df_crimes = pd.read_csv("../data/df_crimes_LSOA_code.csv")
    df_covid = pd.read_csv("../data/covid_restrictions.csv")

    print(df_crimes.head())
    # rename week_start to Month and convert to datetime
    df_covid = df_covid.rename(columns={"week_start": "Month"})
    df_covid["Month"] = pd.to_datetime(df_covid["Month"])
    df_covid["Month"] = df_covid["Month"].dt.strftime("%Y-%m")

    df_crimes.drop(columns=["Unnamed: 0"], inplace=True)
    df_crimes["Month"] = pd.to_datetime(df_crimes["Month"])
    df_crimes["Month"] = df_crimes["Month"].dt.strftime("%Y-%m")

    # merge the dataframes
    df = pd.merge(df_crimes, df_covid[["Month", "restrictions"]], on="Month", how="left")
    df["restrictions"] = df["restrictions"].fillna(0)

    # create seperate month and year columns
    df["Year"] = df["Month"].str[:4]
    df["Month"] = df["Month"].str[5:]

    return df


def encode_data(df):
    label_encoder = LabelEncoder()

    # encode the wards, month and year
    df['wards_encoded'] = label_encoder.fit_transform(df['LSOA code'])
    df['Month_encoded'] = label_encoder.fit_transform(df['Month'])
    df['Year_encoded'] = label_encoder.fit_transform(df['Year'])

    return df


def train_model(df, estimator=10, random_state=53):
    # create model
    model = RandomForestRegressor(n_estimators=estimator, random_state=random_state)

    # X = df[["LSOA code", "Month", "restrictions"]]
    X = df.drop(['Burglary', "LSOA code", "Month", "Year"], axis=1)
    y = df['Burglary']

    X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2)

    model.fit(X_train, y_train)

    y_pred = model.predict(X_test)
    r2 = r2_score(y_test, y_pred)
    mse_score = mse(y_test, y_pred)

    print(f"R2: {r2}, MSE: {mse_score}, Estimators: {estimator}, Random State: {random_state}")

    return model, r2, mse_score


# set up the argument parser
parser = argparse.ArgumentParser(description="Train a random forest model")
parser.add_argument("--estimators", type=int, default=100, help="Number of estimators")
parser.add_argument("--random_state", type=int, default=53, help="Random state")
args = parser.parse_args()
    
def main():
    df = get_data()
    df = encode_data(df)
    train_model(df, args.estimators, args.random_state)


if __name__ == "__main__":
    main()
