
import zipfile
import os
import pandas as pd
import glob

# Name the folder with zip files "zip_files" then all it should all run smoothly

def extract_zip(data_dir='../data/raw'):# Set data directory
    print('extracting zipfiles...')
    # Set the path to the data directory
    data_path = os.path.join(os.getcwd(), data_dir)
    # Create a list of zip files in the data directory
    zip_files = glob.glob(os.path.join(data_path, "*.zip"))
    for zip_file in zip_files:
        with zipfile.ZipFile(zip_file) as myzip:
            for name in myzip.namelist():
                # if "metropolitan" or "hertfordshire"in name then extract the file to the output directory
                if "metropolitan" in name or "hertfordshire" in name:
                    myzip.extract(name, data_dir)

if __name__ == "__main__":
    extract_zip()
