import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error,mean_absolute_error
import xgboost as xgb
import matplotlib.pyplot as plt
import geopandas as gpd
from sklearn.metrics import r2_score
from statsmodels.tsa.stattools import grangercausalitytests
from shapely.geometry import Polygon

def flip_coordinates(geometry):
    # Parse the geometry string into a Shapely Polygon object
    polygon = Polygon(geometry)

    # Extract the coordinates from the polygon
    coords = polygon.exterior.coords

    # Flip the coordinates
    flipped_coords = [(y, x) for x, y in coords]

    # Reconstruct the flipped polygon
    flipped_polygon = Polygon(flipped_coords)

    return flipped_polygon

df = pd.read_csv("../data/df_crimes_LSOA_code.csv")
#df = df.drop('House price', axis=1)
#df = df.drop('Density', axis=1)

#get boundary for LSOA
path = r"../data/LSOA_boundary/LSOA_2011_EW_BFC.shp"
lsoa_london = gpd.read_file(path)
lsoa_london = lsoa_london[['LSOA11CD','geometry']]
lsoa_london = lsoa_london.rename(columns={'LSOA11CD':'LSOA code'})
geometries = lsoa_london['geometry']
lsoa_london['geometry'] = lsoa_london['geometry'].apply(flip_coordinates)
df = df.merge(lsoa_london, on=['LSOA code'])

# get boundary for ward
path = r"../data/df_wards.shp"
df_ward = gpd.read_file(path)
wards_barnet = ["High Barnet", "Underhill", "Barnet Vale", "East Barnet", "Friern Barnet","Woodhouse",
                "Whetstone", "Brunswick Park", "Totteridge and Woodside", "Mill Hill", "Cricklewood",
                 "Edgwarebury", "Burnt Oak", "Colindale South", "West Hendon", "Colindale North","Hendon",
                 "West Finchley", "East Finchley", "Garden Suburb", "Finchley Church End", "Golders Green", "Childs Hill"]
df_ward = df_ward[df_ward['name'].isin(wards_barnet)]
df_ward['geometry'] = df_ward['geometry'].apply(flip_coordinates)

# filter corr
exclude_columns = ["LSOA code", "geometry", "Year", "Month"]
comparison_columns = [column for column in df.columns if column not in exclude_columns]
df[comparison_columns] = df[comparison_columns].fillna(df[comparison_columns].mean())
correlations = df[comparison_columns].corr()["Burglary"]
positive_corr_columns = correlations[correlations >= 0.1].index.tolist()
# filter granger cuasality
for col in positive_corr_columns:
    result = grangercausalitytests(df[[col, 'Burglary']], maxlag=2, verbose=False)
    if result[2][0]['ssr_ftest'][1] <= 0.05:
        exclude_columns.append(col)
exclude_columns.append('Burglary')
df = df[exclude_columns]

le = preprocessing.LabelEncoder()
df['LSOA code'] = le.fit_transform(df['LSOA code'])
df['Month'] = le.fit_transform(df['Month'])


X = df.drop(['Burglary', 'geometry'], axis=1)
y = df['Burglary']
y_log = np.log1p(y)


# Apply scaling
scaler = preprocessing.MinMaxScaler()
X_scaled = scaler.fit_transform(X)

X_train, X_test, y_train, y_test = train_test_split(X_scaled, y_log, test_size=0.2, random_state=42)

# Create DMatrices
dtrain = xgb.DMatrix(X_train, label=y_train)
dtest = xgb.DMatrix(X_test, label=y_test)


# Set parameters for XGBoost
param = {
    'max_depth': 13,
    'eta': 0.3,
    'silent': 1,
    'objective': 'reg:squarederror',
    'alpha': 0.01,
    'lambda': 0.01,
}
num_round = 300

# Train the model
bst = xgb.train(param, dtrain, num_round)
df_predict = df.drop('Burglary', axis=1)

# Initialize an empty DataFrame to store the predicted values for each LSOA code
predictions_df = pd.DataFrame(columns=['LSOA code', 'prediction', 'geometry'])


# Plot residual
lsoa_codes = df_predict['LSOA code'].unique()
actual = np.array([])
predict = np.array([])
# Loop over the unique LSOA codes
for lsoa in lsoa_codes:
    # Filter the data for the specific LSOA code
    specific_data = df[df['LSOA code'] == lsoa].drop('Burglary', axis=1)
    specific_data_scaled = scaler.transform(specific_data.drop('geometry', axis=1))
    d_specific = xgb.DMatrix(specific_data_scaled)
    preds = bst.predict(d_specific)
    preds = np.expm1(preds)

    # Get the actual values for the specific LSOA code
    specific_actual = df[df['LSOA code'] == lsoa]['Burglary']
    actual = np.concatenate((actual,specific_actual))
    predict = np.concatenate((predict,preds))

mae = mean_absolute_error(actual, predict)
mse = mean_squared_error(actual, predict)
r2 = r2_score(actual, predict)

# Calculate the residuals
residuals = actual - predict
# Plot the residuals
plt.scatter(actual, residuals)
plt.axhline(y=0, color='r', linestyle='--')  # Add a horizontal line at y=0 for reference
plt.xlabel('True Values')
plt.ylabel('Residuals')
plt.title(f'Residual plot for last season, MAE: {round(mae,2)}, MSE: {round(mse,2)}, R-squared: {round(r2, 2)}')
plt.show()


# predict each LSOA code
for lsoa in lsoa_codes:
    specific_data = df_predict[df_predict['LSOA code'] == lsoa]
    specific_data_scaled = scaler.transform(specific_data.drop('geometry', axis=1))
    d_specific = xgb.DMatrix(specific_data_scaled)
    preds = bst.predict(d_specific)
    preds = np.expm1(preds)

    # Store the predicted value and geometry in the DataFrame
    row = {'LSOA code': lsoa, 'prediction': preds[0], 'geometry': specific_data['geometry'].iloc[0]}
    # predictions_df =  pd.concat([predictions_df, pd.DataFrame([row])], ignore_index=True) Pandas ^2.0

# print("Predictions for next season:\n", predictions_df)

# Create a GeoDataFrame from predictions_df
predictions_gdf = gpd.GeoDataFrame(predictions_df, geometry='geometry')
print(predictions_gdf['geometry'])

#prediction map
fig, ax = plt.subplots(figsize=(10, 10))
# Plot the polygons
lsoa_london.plot(ax=ax, color='lightgray', edgecolor='blue')
predictions_gdf.plot(ax=ax, column='prediction', cmap='YlOrRd', linewidth=0.8, edgecolor='black', legend=True,
                     vmin=0, vmax=10)
df_ward.plot(ax=ax, color='none', edgecolor='black', linewidth=3)

# Set aspect ratio and axis limits
ax.set_aspect('equal')

# Remove the axis labels
ax.set_axis_off()

ax.set_title('Burglary predictions for next month')
#plt.show()
