import numpy as np
import pandas as pd
import json
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import r2_score
import torch
import torch.nn as nn
from models import AttentionGCN, RecurrentGCN, GCNGCN, ChebyshevGCN, AttentionGCNl
from torch_geometric_temporal import  GConvLSTM, GConvGRU
from torch_geometric.nn.conv import CuGraphGATConv
from torch_geometric.loader import DataLoader
from torch_geometric.data import InMemoryDataset, Data
import os
from tqdm import tqdm
from torch_geometric.data import InMemoryDataset, Data


class CrimeDataset(InMemoryDataset):
    def __init__(self, root, transform=None, pre_transform=None):
        super(CrimeDataset, self).__init__(root, transform, pre_transform)
        self.data, self.slices = torch.load(self.processed_paths[0])

    @property
    def raw_file_names(self):
        return ['graph_data.pt']

    @property
    def processed_file_names(self):
        return ['processed_data.pt']

    def download(self):
        pass

    def process(self):
        data = torch.load(self.raw_paths[0])
        data_list = []
        for t in range(1, data.x.size(1)):
            x_t = data.x[:, t-1, :]  # Node features at previous timestep
            y_t = data.y[:, t]  # 'Burglary' to predict at current timestep
            edge_index_t = data.edge_index[:, data.edge_index[1] < t]  # only keep edges up to current timestep
            edge_attr_t = data.edge_attr[data.edge_index[1] < t]
            data_t = Data(x=x_t, edge_index=edge_index_t, edge_attr=edge_attr_t, y=y_t)
            data_list.append(data_t)

        data, slices = self.collate(data_list)
        torch.save((data, slices), self.processed_paths[0])


dataset = CrimeDataset(root='../data/')
dataloader = DataLoader(dataset, batch_size=1)



# Create an instance of the model
#model = GConvLSTM(in_channels=16, out_channels=1, K=10)
#model = GCNGCN(node_features=16, filters=1)
#model = ChebyshevGCN(node_features=16, filters=1, K=3)

model = AttentionGCNl(node_features=17, filters=16, heads=10)
#model = CuGraphGATConv(
#        in_channels = 16,1
#        out_channels = 1,
#        heads = 3,
#        concat = True,
#        negative_slope = 0.2,
#        bias = True)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = model.to(device)
print(f'initiating model on {device}')


# Define a loss function and an optimizer
loss_fn = nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters(), lr=3e-3, weight_decay=0.001, amsgrad=True)
num_epochs = 20

losses, r2_scores, mses = [], [], []
all_predictions, all_true_values = [], []
# Train the model
for epoch in tqdm(range(num_epochs), desc='Training'):
    epoch_losses, epoch_r2_scores = [], []
    for data in dataloader:
        # Extract the data
        x, edge_index, edge_attr, y = data.x, data.edge_index, data.edge_attr.float(), data.y
        x, edge_index, edge_attr, y = x.to(device), edge_index.to(device), edge_attr.to(device), y.to(device)
        # Zero the gradients
        optimizer.zero_grad()

        # Forward pass
        H = model(x, edge_index)
        #H = model(x, edge_index, edge_attr)
        # H, c

        if epoch == num_epochs - 1:
            all_predictions.append(H.detach().cpu().numpy())
            all_true_values.append(y.unsqueeze(1).detach().cpu().numpy())

        # Compute the loss
        loss = loss_fn(H, y.unsqueeze(1).to(device))
        epoch_losses.append(loss.item())


        # Compute metrics
        r2_score_value = r2_score(y.cpu().numpy(), H.detach().cpu().numpy())
        epoch_r2_scores.append(r2_score_value)

        # Backward pass
        loss.backward()

        # Update the weights
        optimizer.step()

    losses.append(np.mean(epoch_losses))
    r2_scores.append(np.mean(epoch_r2_scores))

    if epoch % 2 == 0:
        tqdm.write(f"{'-'*20}\nEpoch : {epoch}\nLoss (MSE) = {losses[-1]:.4f}\nR2 Score = {r2_scores[-1]:.4f}")


all_predictions = np.concatenate(all_predictions, axis=1)
all_true_values = np.concatenate(all_true_values, axis=1)

y_true = pd.DataFrame(all_true_values.T)
y_pred = pd.DataFrame(all_predictions.T)

with open('../data/mappings.json', 'r') as f:
    mapping_dict = json.load(f)
    mapping_dict = mapping_dict['index_to_lsoa']
    print( mapping_dict['0'])

def rename_columns(old_name):
    try:
        x = mapping_dict[str(old_name)]
        return x
    except KeyError:
        print(f'number: {old_name}')
        return old_name

y_true_cols = {col: rename_columns(col) for col in y_true.columns}
y_pred_cols = {col: rename_columns(col) for col in y_pred.columns}
y_true.rename(columns=y_true_cols, inplace=True)
y_pred.rename(columns=y_pred_cols, inplace=True)


num_months = y_true.shape[0]
y_true.index = pd.date_range(start='2010-04', periods=num_months, freq='M')
y_pred.index = pd.date_range(start='2010-04', periods=num_months, freq='M')

y_true.to_csv('../data/y_true.csv')
y_pred.to_csv('../data/y_pred.csv')

# Initialize a scaler with a range of 0-1
scaler = MinMaxScaler(feature_range=(0, 1))

# Fit and transform the metrics to the scaler
losses_scaled = scaler.fit_transform(np.array(losses).reshape(-1, 1)).flatten()
r2_scores_scaled = scaler.fit_transform(np.array(r2_scores).reshape(-1, 1)).flatten()

# Plotting the training metrics
plt.figure(figsize=(10,5))
plt.plot(losses, label='Loss')
plt.plot(r2_scores, label='R2 Score')
plt.title('Training metrics')
plt.xlabel('Epochs')
plt.ylabel('Value')
plt.legend()
#plt.show()
plt.savefig('../images/loss.png')
