# JBG040-Group9
This repository contains the code for the TU/e course JBG050 Data Challenge 2. Please read this document carefully as it has been filled out with important information on how to run the code.

## Get Started
This readme is tested for Linux, macOS & Windows inside WSL (Windows Subsystem for Linux)
The virtual environment and python dependencies are handled py Poetry, if you don't have Poetry installed, install it by running this command in the terminal:
```
curl -sSL https://install.python-poetry.org | python3 -
```

With poetry installed, to get started working on this open a terminal, type in `cd <theDirectory/youWant/toWorkin>`, then only the first time you download the repo do:
```
git clone https://gitlab.com/barryboy/jbg050-group9.git
cd JBG040-Group19/
poetry install
poetry shell
pip install torch
pip install torch-sparse torch-scatter
pip torch-geometric-temporal
```
this downloads the repository, creates a new environment for this project and then installs all the dependencies, torch & torch-geometric-temporal are not tracked by Poetry since they require being manually installed. Now anytime you want to run anything related to this project before make sure you are in the JBG050-Group9 folder, if you are not do `cd /path/to/JBG050-Group9` and activate the correct environment by typing in the terminal `poetry shell`.


<!-- For pytorch_geometric, Pytorch 1.12.0 or above is required.
https://pytorch-geometric.readthedocs.io/en/latest/install/installation.html
-->


## Download data

Now to download the data go to `https://data.police.uk/data/`, click on Custom Dowload.

Then select the following:


![alt text](https://gitlab.com/barryboy/jbg050-group9/-/raw/875f057271373e99f2886d09673780c0b5e9825c/images/data.png)

Then go on `https://data.police.uk/data/archive/` by clicking on archive and download the following:
- `April 2020`
- `April 2017`

then copy the 3 zipfiles in the `data/raw/` folder of this repository.

This is data from December 2010 to March 2023, we will consider the Metropolitan area and Hertfordshire

Now to prepare the data in that terminal run:

```
cd jbg050/
python3 process_crimes.py
python3 graph_preprocessing.py
```
you can choose between wards and LSOA codes as aggregation level, to generate `df_wards.csv` run the `process_crimes.py` with Wards aggregation level

this will save the resulting datasets in the `/data/` folder as `df_crimes_{aggregation level}.csv`

## Run the models
Now instructions to replicate running the models:
- xgboost: all the code regarding xgboost is contained in `xgb.py`, run the model with `python3 xgb.py`
- random forest: all the code regarding random forest is contained in `random_forest.py`, run the model with `python3 random_forest.py`
  - Arguments: `python3 random_forest.py --estimators <int> --random_state <int>` for parameters. Default values are 10 and 53 respectively.
- Graph Neural nets: the code regarding GNNs is structured this way:
  - `run.py` the script used to replicate results, you should run this
    - `main.py` main script that runs the training, supports wandb and CLI args, to list all options run `python3 main.py --help`
    - `parser.py` which contains the CLI arguments parsing function
    - `train_test.py` contains the train and test loop functions
    - `models.py` contains the various Graph model classes
    - `dataset.py` contains the torch_geometric dataset classes used for training
